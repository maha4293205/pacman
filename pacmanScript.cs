using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class pacmanScript: MonoBehaviour
{
    public float moveSpeed = 5f; 
    private Rigidbody rb;
    private int score = 0;
    public Text ch;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        UpdateScore();
    }

    private void Update()
        {
      
        float mh = Input.GetAxis("Horizontal");
        float mv = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(mh, 0f, mv).normalized;

        if (movement != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(movement);
        }

     
        transform.Translate(movement * moveSpeed * Time.deltaTime, Space.World);
    }



    private void OnCollisionEnter(Collision collision)
    {
         if (collision.collider.tag.Equals("gome"))
        {
            Destroy(collision.gameObject);
            score++;
            Debug.Log(score);
            UpdateScore();

        }
        else if (collision.collider.tag.Equals("Wall"))
        {
            Debug.Log("collider");
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            
        }
    }
     private void UpdateScore()
    {
        if (ch != null)
        {
            ch.text = "Score: " + score.ToString();
        }
    }
}
